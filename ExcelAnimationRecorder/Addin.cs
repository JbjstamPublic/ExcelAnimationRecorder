﻿using System;
using System.Runtime.InteropServices;
using NetOffice.Tools;
using NetOffice.ExcelApi.Tools;
using Excel = NetOffice.ExcelApi;
using Office = NetOffice.OfficeApi;
using System.Linq;
using System.Collections.Generic;

namespace ExcelAnimationRecorder
{
    [COMAddin("ExcelAnimationRecorder", "Excel Animation Recorder", LoadBehavior.LoadAtStartup)]
    [CustomUI("RibbonUI.xml", true)]
    [CustomPane(typeof(YourCode), "Your code", false, PaneDockPosition.msoCTPDockPositionTop, PaneDockPositionRestrict.msoCTPDockPositionRestrictNoChange, 60, 100)]
    [ProgId("ExcelAnimationRecorder"), Guid("F37EB2DC-3B7C-4DAD-A82D-26EE0CDFE8B5"), Codebase]
    public partial class Addin : COMAddin
    {
        private static Excel.Application _app = null;
        private double _excelVersion;
        private static List<Excel.Shape> _lastSelectedShapes = new List<Excel.Shape>();

        internal Office.IRibbonUI RibbonUI { get; private set; }

        public Addin()
        {
            OnConnection += Addin_OnConnection;
            OnDisconnection += Addin_OnDisconnection;
        }

        private void Addin_OnDisconnection(ext_DisconnectMode removeMode, ref Array custom)
        {
            _app.CommandBars.OnUpdateEvent -= CommandBars_OnUpdateEvent;
            _app.SheetActivateEvent -= SheetActivateEvent;
        }

        private void Addin_OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
        {
            _app = (Excel.Application)application;

            _app.CommandBars.OnUpdateEvent += CommandBars_OnUpdateEvent;
            _app.SheetActivateEvent += SheetActivateEvent;

            _excelVersion = double.Parse(_app.Version, System.Globalization.CultureInfo.InvariantCulture);
        }

        public void OnLoadRibonUI(Office.IRibbonUI ribbonUI)
        {
            RibbonUI = ribbonUI;
        }

        private void SheetActivateEvent(NetOffice.COMObject sheet)
        {
            RibbonUI.Invalidate();
        }

        /// <summary>
        /// Refreshes the shape dorp down whenever the shape selection changes. The event is fired
        /// every time that the CommandBars are updated.
        /// </summary>
        private void CommandBars_OnUpdateEvent()
        {
            var selectedShapes = _app.GetSelectedShapes();

            if ((selectedShapes.Count != _lastSelectedShapes.Count && selectedShapes.Count != 0) || _lastSelectedShapes.SequenceEqual(selectedShapes))
                RefreshShapesDropDown();

            _lastSelectedShapes = selectedShapes;
        } 
    }   
}
//imageMso="MacroRecord" for built-in images