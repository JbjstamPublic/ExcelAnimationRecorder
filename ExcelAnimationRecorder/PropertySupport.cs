﻿using System;
using NetOffice;
using Excel = NetOffice.ExcelApi;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;

namespace ExcelAnimationRecorder
{
    class PropertySupport
    {
        private static Dictionary<string, List<PropertyInfo>> _supportedProperties = null;
        public static readonly Regex IndexerRegex = new Regex(@"^(.*?)\[(.*?)\]$", RegexOptions.IgnoreCase | RegexOptions.Singleline);

        public static List<PropertyInfo> GetSupportedProperties(Type type, double compareVersion)
        {
            if (_supportedProperties == null)
                _supportedProperties = new Dictionary<string, List<PropertyInfo>>();

            if (_supportedProperties.ContainsKey(type.FullName))
                return _supportedProperties[type.FullName];

            var supportedProperties = type.GetProperties().Where(p =>
            {
                var supportAttribute = p.GetCustomAttributes(typeof(SupportByVersionAttribute), true)?.Cast<SupportByVersionAttribute>().FirstOrDefault();

                if (supportAttribute == null)
                    return true;

                var lowestSupportedVersion = supportAttribute.Versions.Min();

                return lowestSupportedVersion <= compareVersion;   
                            
            }).ToList();

            _supportedProperties.Add(type.FullName, supportedProperties);

            return supportedProperties;
        }

        public class PropAndIndexer
        {
            public string PropName { get; set; }
            public string Indexer { get; set; }

            public PropAndIndexer(string fullProp)
            {
                var match = IndexerRegex.Match(fullProp);
                if (match.Success)
                {
                    PropName = match.Groups[1].Value;
                    Indexer = match.Groups[2].Value;
                }
                else
                {
                    PropName = fullProp;
                    Indexer = string.Empty;
                }
            }

            public override string ToString()
            {
                return string.IsNullOrEmpty(Indexer) ? PropName : $"{PropName}[{Indexer}]";
            }
        }

        /// <summary>
        /// Makes sure that all properties in the chain exists
        /// Additionally, it normalizes the names of the properties (capital letters)
        /// </summary>
        /// <param name="chain"></param>
        /// <param name="compareVersion"></param>
        public static List<PropAndIndexer> ValidatePropertyChain(List<string> chain, double compareVersion)
        {
            var indexedChain = chain.Select(s => new PropAndIndexer(s)).ToList();

            ValidatePropertyChain(indexedChain, 0, typeof(Excel.Shape), compareVersion);

            return indexedChain;
        }

        private static void ValidatePropertyChain(List<PropAndIndexer> chain, int index, Type type, double compareVersion)
        {
            if (chain == null || chain.Count <= index)
                throw new Exception($"The property chain is too short or null");

            var propName = chain[index].PropName;

            var propertyInfo = GetSupportedProperties(type, compareVersion).FirstOrDefault(p => p.Name.Equals(propName, StringComparison.InvariantCultureIgnoreCase));

            if (propertyInfo == null)
                throw new Exception($"The property {propName} does not exist");

            chain[index].PropName = propertyInfo.Name;

            if (chain.Count > ++index)
                ValidatePropertyChain(chain, index, propertyInfo.PropertyType, compareVersion);
            
            else
                ThrowIfNotSupportedProperty(propertyInfo);
        }

        public static void ThrowIfNotSupportedProperty(PropertyInfo propertyInfo)
        {
            var propertyType = propertyInfo.PropertyType;

            if (IsSupportedPrimitive(propertyType))
            {

                if (!propertyInfo.CanWrite)
                    throw new Exception($"{propertyInfo.Name} is read-only");
            }
            else
            {

                var IsIndexer = propertyType.GetProperties().Any(m => m.GetIndexParameters().Any());
                if (!IsIndexer)
                {
                    throw new Exception($"{propertyInfo.Name} is not a primitive type (including strings and enums)");
                }
            }
        }

        private static bool IsSupportedPrimitive(Type propertyType)
        {
            return propertyType.IsPrimitive || propertyType == typeof(string) || propertyType.IsEnum;
        }
    }
}
