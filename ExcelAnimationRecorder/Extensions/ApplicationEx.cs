﻿using System.Collections.Generic;
using System.Linq;
using Excel = NetOffice.ExcelApi;

namespace ExcelAnimationRecorder
{
    static class ApplicationEx
    {
        public static List<Excel.Shape> GetSelectedShapes(this Excel.Application app)
        {
            var shapeRangeProperty = app.Selection.GetType().GetProperty("ShapeRange");
            var shapeRange = (Excel.ShapeRange)shapeRangeProperty?.GetValue(app.Selection, null);
            return !Equals(shapeRange, null) ? shapeRange.ToList() : new List<Excel.Shape>();
        }

        public static Excel.Worksheet GetActiveSheet(this Excel.Application app)
        {
            return (Excel.Worksheet)app.ActiveSheet;
        }
    }
}
