﻿using Microsoft.Deployment.WindowsInstaller;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelAnimationRecorderInstaller
{
    public class CustomActions
    {
        //session.Log("Registering DLL");

        [CustomAction]
        public static ActionResult Install(Session session)
        {
            var platform = GetPlatform(session);

            try {
                var isInstaller64 = platform.Trim().Equals("x64", StringComparison.InvariantCultureIgnoreCase);
                var excelBitness = ExcelBitness.GetExcelBitness();
                var isExcel64 = excelBitness == ExeType.Bit64;

                if (excelBitness == ExeType.Invalid || isInstaller64 != isExcel64)
                {
                    var msg = $"Installed version of Excel isnt {platform}.{Environment.NewLine}Please try the other installer.";
                    MessageBox.Show(msg, "", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return ActionResult.Failure;
                }

                var regasmPath = GetRegAsmPath();
                var allDlls = GetDlls(session);
                var dlls = allDlls.Where(dll => IsComDll(dll).Item1);


                MessageBox.Show("Registering DLLs: " + string.Join(Environment.NewLine, dlls) + Environment.NewLine +
                    "RegAsm: " + regasmPath + Environment.NewLine +
                    "Platform: " + platform + Environment.NewLine +
                    "All DLLs: " + string.Join(Environment.NewLine, allDlls)
                    );

                foreach (var dll in dlls)
                {
                    RegisterDll(dll, regasmPath);
                }

                return ActionResult.Success;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

                return ActionResult.Failure;
            }
        }

        [CustomAction]
        public static ActionResult Uninstall(Session session)
        {
            try {
                var regasmPath = GetRegAsmPath();
                var platform = GetPlatform(session);
                var allDlls = GetDlls(session);
                var dlls = allDlls.Where(dll => IsComDll(dll).Item2);
                MessageBox.Show("Unregistering DLLs: " + string.Join(Environment.NewLine, dlls) + Environment.NewLine +
                    "RegAsm: " + regasmPath + Environment.NewLine +
                    "Platform: " + platform + Environment.NewLine +
                    "All DLLs: " + string.Join(Environment.NewLine, allDlls)
                    );

                foreach (var dll in dlls)
                {
                    UnregisterDll(dll, regasmPath);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }


            return ActionResult.Success;
        }

        private static void RegisterDll(string dll, string regasmPath)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = regasmPath,
                    Arguments = $"/codebase {dll}",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            var output = new StringBuilder();

            process.Start();

            while (!process.StandardOutput.EndOfStream)
            {
                output.AppendLine(process.StandardOutput.ReadLine());
            }

            MessageBox.Show(output.ToString(), dll);

        }

        private static void UnregisterDll(string dll, string regasmPath)
        {
            try {
                if (!File.Exists(dll))
                {
                    MessageBox.Show(dll + "  no longer exists, skipped");
                    return;
                }

                var process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = regasmPath,
                        Arguments = $"/u {dll}",
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                var output = new StringBuilder();

                process.Start();

                while (!process.StandardOutput.EndOfStream)
                {
                    output.AppendLine(process.StandardOutput.ReadLine());
                }

                MessageBox.Show(output.ToString(), dll);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private static Tuple<bool, bool> IsComDll(string dll)
        {
            try {
                var assembly = Assembly.LoadFrom(dll);
                var types = assembly.GetTypes();
                var type = types.FirstOrDefault(t => t.CustomAttributes.Any(a => a.AttributeType == typeof(ProgIdAttribute)));
                var staticPublicMethods = type.GetMethods(BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Static);
                var hasRegisterFunction = staticPublicMethods.Any(m => m.CustomAttributes.Any(a => a.AttributeType == typeof(ComRegisterFunctionAttribute)));
                var hasUnregisterFunction = staticPublicMethods.Any(m => m.CustomAttributes.Any(a => a.AttributeType == typeof(ComRegisterFunctionAttribute)));
                return new Tuple<bool, bool>(hasRegisterFunction, hasUnregisterFunction);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return new Tuple<bool, bool>(false, false);
            }
        }

        private static string GetPlatform(Session session)
        {
            MessageBox.Show(string.Join(Environment.NewLine, session.CustomActionData.Values));
            return session.CustomActionData["PLATFORM"];
        }

        private static string GetRegAsmPath()
        {
            return Path.Combine(RuntimeEnvironment.GetRuntimeDirectory(), "regasm.exe");
        }

        private static string[] GetDlls(Session session)
        {
            MessageBox.Show(session.CustomActionData["DLLFILES"]);
            return session.CustomActionData["DLLFILES"].Split(';');
        }
    }
}